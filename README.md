# Puffer
### Discord bot for game server management.
It contains features of moderation, trade announcements, voice chat management, as well as a member gateway using Discord's UI forms (WIP).

There is not a universal instance of Puffer, because the bot is made to be customizable for each server. So you're going to have to **create and host your instance** if you want the bot for your server.

## Installation
Puffer has several steps to installing on your server. Prerequisites:

First, you need Python 3.11+.
Second, if you choose to enable the optional DB-using [cogs](docs/cogs.md), you'll need to have a Redis database running somewhere that you can connect to.

Now you can install the bot:

1. Head over to the [Discord Developers](https://discord.com/developers) page, create a bot and grab it's token. You'll find a plenty of instructions on how to do it online. Don't forget to disable invites not-by-owner.
2. Invite your bot to the server that you're going to use it in. As well don't forget to create required channels, if [the cogs](docs/cogs.md) you'll be using require any. **Remember, one instance covers one server.**
2. Clone the repository to your hosting computer.
3. Copy **(don't move/rename, copy)** the `.env.example` file to `.env` and **edit it** with your values. There are short explanations for the environment variables in the example file, but the long descriptions can be read in [the Config documentation](docs/config.md).
4. Proceed to create a virtual environment and launch the bot with the following commands:
```sh
python -m venv .venv
source .venv/bin/activate
pip install .
python puffer
```
5. If you did everything correctly, the bot should be running!

If your members can't use commands - make sure they have the Connect permission. We recommend removing this permission from the default (@everyone) role, especially if you're using Verification features.

## Cogs and Configuration
You can read about what each cog does and what each entry in the `.env` file is in [Cogs documentation](docs/cogs.md) and [Config documentation](docs/config.md).

> [!NOTE]
> Currently you cannot customize the fields for the verification modal, as well as the placeholder text for the greeting message. This should be fixed by the 1.0.0 release.

