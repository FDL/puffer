## Cogs
Main functionality of Puffer is located in cogs. You can toggle them in the `.env` file, if you don't need certain functionality for your server. Here's what each of them does:

### Moderation
The Moderation cog allows your moderators/admins to timeout members, slowmode channels and give warns to members. The warn system is made, so that when [WARN_LIMIT](config.md#warn_limit) is reached for a certain member, they will be banned. A Redis DB connection is **required** for it.

Required permissions:
- `/timeout` - Moderate Members
- `/slow` - Manage Channels
- `/warn` - Ban Members

### Modmail
The Modmail cog allows your server's members to send messages to moderators/admins via the [MODMAIL_CHANNEL_ID](config.md#modmail_channel_id) channel, once every 5 minutes per member. A Redis DB connection is **not required** for it.

Required permissions:
- `/modmail` - Connect

### Trade
The Trade cog allows your members to send trade (buy/sell) requests to the [TRADE_CHANNEL_ID](config.md#trade_channel_id) channel. Members can send requests from anywhere in the server. For each request created, a thread will get created for it to be discussed. A Redis DB connection is **not required** for it.

Required permissions:
- `/trade` - Connect

### Voice
The Voice cog allows your members to create dynamic voice channels, that delete themselves when nobody is using them. They are created by joining the [VOICE_CHANNEL_ID](config.md#voice_channel_id) channel. Members can also change the default name of their created channel. A Redis DB connection is **required** for it.

Required permissions:
- `/vcname` - Connect

### Verification
The Verification cog allows server owners to accept/decline their users's applications, sent through [VERIFICATION_CHANNEL_ID](config.md#verification_channel_id) to [MODMAIL_CHANNEL_ID](config.md#modmail_channel_id). Upon accepting, the user that sent the application is granted the [PLAYER_ROLE_ID](config.md#player_role_id) role, which should be configured by admins, to have access to most chats AND have the Connect permission. A Redis DB connection is **required** for it.