## Configuration
The `.env` file is a configuration file, containing variables, that you can change for the bot. This section describes each one of them. Examples are given in the `.env.example` file, which you copy and edit as shown in the installation tutorial. If you aren't using a specific variable, you can leave it as-is, as long as it matches the type.

### TOKEN
> Type: String

The token for the Discord bot, recieved from the [Discord Developers](https://discord.com/developers) website. You should be able to find plenty of tutorials on the internet covering it.

### ACTIVITY_NAME
> Type: String

The name of the activity that will be displayed in the bot's status.  
For example: "Playing a game" if the ACTIVITY_NAME envar is set to "a game".

### COLOR
> Type: Base-16 Integer

The hexadecimal color code used for the embeds.  
For example: 0xABCDEF will result in a light blue-ish color.

### WARN_LIMIT
> Type: Integer

The amount of warns, upon reaching which, the member is banned from the server. This is a part of the warn system implemented by the [Moderation](cogs.md#moderation) cog.

### GUILD_ID
> Type: ID (Integer)

The ID of the guild, that the bot will be running on. This is used to quickly update the slash commands on your server.

### TRADE_CHANNEL_ID
> Type: ID (Integer)

The ID of the channel, that trade requests will be sent to by the [Trade](cogs.md#trade) cog. It is recommended to disable talking in this channel, but leaving talking in threads, as they are created under each post.

### VOICE_CHANNEL_ID
> Type: ID (Integer)

The ID of the voice channel, that members can join to create a voice channel in the same category. Used by the [Voice](cogs.md#voice) cog. The channel can have any name, but we recommend something understandable, like `[+] Create`.

### VERIFICATION_CHANNEL_ID
> Type: ID (Integer)

The ID of the channel, that will have the greeting message from the bot and the Apply button. This channel should only be visible to people without a role that just joined. Used by the [Verification](cogs.md#verification) cog.

### MODMAIL_CHANNEL_ID
> Type: ID (Integer)

The ID of the channel, that is only visible to moderators/admins. Modmail and applications by new members will be sent there. Used by the [Modmail](cogs.md#modmail) and [Verification](cogs.md#verification) cogs.

### PLAYER_ROLE_ID
> Type: ID (Integer)

The ID of the role, that is given to members upon their application being accepted by the moderators. Recommened to have a name like "Player" or "Verified". Used by the [Verification](cogs.md#verification) cog.

### REDIS_URL
> Type: URI (String)

The URI of the Redis Database, that some cogs of the bot will connect to. It's formatted as `redis://user:password@ip:port`. If you are running the database on the same system as the bot, you'll probably not require a user:password combination, but if you have your database open to the internet - Redis will require you to set a password, the user can be omitted if the Redis config allows it.

### ENABLED_COGS
> Type: String

A space-separated list of cogs, that your instance of the bot is going to use. If you want to enable all cogs - use `"*"`. Or list just the cogs you need, like `"Modmail Trade"`, that will enable only the [Modmail](cogs.md#modmail) and [Trade](cogs.md#trade) cogs. The list of cogs can be seen in the [Cogs](cogs.md) documentation file.