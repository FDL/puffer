import logging
import redis.asyncio as redis
import disnake
from disnake.ext.commands import InteractionBot

import puffer

puffer.util.setup_logging()

intents: disnake.Intents = disnake.Intents.default()
intents.members = True
intents.message_content = True
bot = InteractionBot(test_guilds=[puffer.config.GUILD_ID], intents=intents)

logging.getLogger("puffer").info(f"Enabled cogs: {puffer.config.ENABLED_COGS}")
ENABLED_COGS = puffer.config.ENABLED_COGS

db = None
DB_COGS = ["Moderation", "Voice", "Verification"]
for cog in ENABLED_COGS:
    if cog in DB_COGS or ENABLED_COGS[0] == "*":
        db = redis.Redis.from_url(puffer.config.REDIS_URL, decode_responses=True)
        logging.getLogger("puffer").info("Redis client object created")
        break


def load_cog(cls, *args, **kwargs):
    if cls.__name__ in ENABLED_COGS or ENABLED_COGS[0] == "*":
        bot.add_cog(cls(*args, **kwargs))


load_cog(puffer.cogs.Trade)
load_cog(puffer.cogs.Moderation, db)
load_cog(puffer.cogs.Voice, db)
load_cog(puffer.cogs.Verification, db, bot)
load_cog(puffer.cogs.Modmail)


@bot.event
async def on_ready():
    game = disnake.Game(puffer.config.ACTIVITY_NAME)
    await bot.change_presence(activity=game)
    logging.getLogger("puffer").info(f"Launched as {bot.user}")


bot.run(puffer.config.TOKEN)
