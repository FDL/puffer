import logging
import redis.asyncio as redis
from datetime import datetime, timedelta
from time import time
import disnake
from disnake.ext import commands
from json import loads, dumps
from puffer.config import WARN_LIMIT


class Moderation(commands.Cog):
    def __init__(self, db):
        self.logger = logging.getLogger("puffer.cogs.moderation")
        self.db: redis.Redis = db

    @commands.slash_command(
        description="Отстранить пользователя на Х минут",
        default_member_permissions=disnake.Permissions(moderate_members=True),
    )
    async def timeout(
        self,
        inter,
        member: disnake.Member = commands.Param(
            description="Пользователь для отстранения; не владелец или администратор",
        ),
        minutes: int = commands.Param(
            ge=1,
            le=40320,
            description="На сколько минут отстранить; не более 40320",
        ),
        reason: str = commands.Param(
            description="Причина отстранения (показывается в Аудит Логе)",
        ),
    ):
        if member.guild_permissions.administrator:
            await inter.send(
                "Нельзя отстранить: пользователь - Администратор",
                ephemeral=True,
            )
        elif inter.guild.owner == member:
            await inter.send(
                "Нельзя отстранить: пользователь - Владелец сервера",
                ephemeral=True,
            )
        else:
            await member.timeout(
                duration=minutes * 60,
                reason=f"{inter.author}: {reason}",
            )

            ts = round(datetime.timestamp(datetime.now() + timedelta(minutes=minutes)))
            await inter.send(
                f"Отстранил {member.mention} на `{minutes}` минут до <t:{ts}:f>"
            )

            self.logger.info(f"{inter.author} timed out {member} for {minutes}m")

    @commands.slash_command(
        description="Установить слоумод на канале",
        default_member_permissions=disnake.Permissions(manage_channels=True),
    )
    async def slow(
        self,
        inter,
        channel: disnake.TextChannel,
        seconds: int = commands.Param(
            ge=0,
            le=21600,
            description="Максимум 21600 секунд (6ч), 0 для отключения",
        ),
    ):
        await channel.edit(
            reason=f"Установлено {inter.author}",
            slowmode_delay=seconds,
        )

        if seconds > 0:
            await inter.send(
                f"Установил слоумод для <#{channel.id}> на `{seconds}` секунд"
            )
        else:
            await inter.send(f"Убрал слоумод для <#{channel.id}>")

        self.logger.info(
            f"{inter.author} set slowmode to {seconds}s in channel #{channel}"
        )

    @commands.slash_command(
        default_member_permissions=disnake.Permissions(ban_members=True)
    )
    async def warn(self, inter):
        pass

    @warn.sub_command(description="Добавить варн пользователю")
    async def add(self, inter, member: disnake.Member, reason: str):
        try:
            uwarns = loads(await self.db.get(f"users:{member.id}"))
        except TypeError:
            uwarns: list[dict] = []
        warnd = {"reason": reason, "timestamp": int(time())}
        uwarns.append(warnd)
        await self.db.set(f"users:{member.id}", dumps(uwarns))
        await inter.send(
            f"Варн {member.mention} успешно добавлен!\n"
            + f"Это их {len(uwarns)} варн из {WARN_LIMIT}.",
            ephemeral=True,
        )
        await member.send(
            f"Вам был выдан варн модератором на сервере {inter.guild} по причине `{warnd['reason']}`.\n"
            + f"Это ваш **{len(uwarns)} варн из {WARN_LIMIT}**."
        )
        self.logger.info(
            f"{member} given warn {len(uwarns)}/{WARN_LIMIT} by {inter.author}"
        )
        if len(uwarns) >= WARN_LIMIT:
            await member.send(
                f"Вы достигли лимита варнов. Вы забанены с {inter.guild}."
            )
            await member.ban(reason=f"Достиг лимита варнов ({WARN_LIMIT})")
            await inter.send(f"{member.mention} забанен по достижению лимита.")
            self.logger.info(f"{member} banned for reaching warn limit")

    @warn.sub_command(description="Список варнов пользователя")
    async def list(self, inter, member: disnake.Member):
        try:
            uwarns = loads(await self.db.get(f"users:{member.id}"))
        except TypeError:
            uwarns: list[dict] = []
        if len(uwarns) == 0:
            await inter.send(f"У {member.mention} нет варнов.", ephemeral=True)
        else:
            warnlist = ""
            for i in uwarns:
                warnlist += f"**{uwarns.index(i)+1}.** Причина: `{i['reason']}`, время: <t:{i['timestamp']}:f>\n"
            await inter.send(
                f"У {member.mention} {len(uwarns)} варнов:\n\n" + warnlist,
                ephemeral=True,
            )
            self.logger.info(f"{inter.author} listed warns of {member}")

    @warn.sub_command(description="Убрать варн у пользователя (n - из /warn list)")
    async def remove(self, inter, member: disnake.Member, n: int):
        try:
            uwarns = loads(await self.db.get(f"users:{member.id}"))
        except TypeError:
            inter.send(f"У {member.mention} нет варнов.", ephemeral=True)
            return
        if len(uwarns) <= n:
            uwarns.pop(n - 1)
            await self.db.set(f"users:{member.id}", dumps(uwarns))
            await inter.send(
                f"Варн {n} у {member.mention} успешно удалён!", ephemeral=True
            )
            await member.send(
                f"Модератор сервера {inter.guild} удалил ваш варн {n}.\n"
                + f"У вас осталось {len(uwarns)} варнов из {WARN_LIMIT}."
            )
            self.logger.info(f"{inter.author} removed warn {n} from {member}")
        else:
            await inter.send(
                f"У {member.mention} нет столько варнов.\n"
                + "Их можно посмотреть через `/warn list`",
                ephemeral=True,
            )

    @warn.sub_command(description="Убирает ВСЕ варны у пользователя")
    async def removeall(self, inter, member: disnake.Member):
        await self.db.set(f"users:{member.id}", dumps([]))
        await inter.send(f"Все варны {member.mention} успешно удалены!", ephemeral=True)
        self.logger.info(f"{inter.author} removed ALL warns from {member}")
