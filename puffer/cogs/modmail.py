import logging
import disnake
from disnake.ext import commands
import puffer


class Modmail(commands.Cog):
    def __init__(self):
        self.logger = logging.getLogger("puffer.cogs.modmail")

    @commands.cooldown(rate=1, per=300, type=commands.BucketType.member)
    @commands.slash_command(
        default_member_permissions=disnake.Permissions(connect=True),
        description="Отправить личное сообщение администраторам",
    )
    async def modmail(self, inter, message: str):
        channel = inter.bot.get_channel(puffer.config.MODMAIL_CHANNEL_ID)
        embed = disnake.Embed(
            title="Сообщение",
            description=message,
            color=puffer.config.COLOR,
        )
        embed.set_author(name=inter.author, icon_url=inter.author.avatar.url)
        msg = await channel.send(embed=embed)
        await msg.create_thread(name="Обсуждение")
        await inter.send(
            "Сообщение успешно отправлено администраторам!", ephemeral=True
        )
        self.logger.info(f"{inter.author} sent a modmail message")

    @modmail.error
    async def modmail_error(self, inter, error):
        if isinstance(error, commands.CommandOnCooldown):
            await inter.send(
                "Отправлять сообщение можно только раз в 5 минут.", ephemeral=True
            )
