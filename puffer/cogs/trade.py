import logging

import disnake
from disnake.ext import commands

import puffer


class Trade(commands.Cog):
    def __init__(self):
        self.logger = logging.getLogger("puffer.cogs.trade")

    async def __send_trade(self, inter, title: str, description: str):
        channel = inter.bot.get_channel(puffer.config.TRADE_CHANNEL_ID)
        embed = disnake.Embed(
            title=title, description=description, color=puffer.config.COLOR
        )
        embed.set_footer(text=inter.author, icon_url=inter.author.avatar.url)
        msg = await channel.send(embed=embed)
        await msg.create_thread(name="Обсуждение")
        await inter.send(
            f"Запрос отправлен в <#{puffer.config.TRADE_CHANNEL_ID}> успешно",
            ephemeral=True,
        )

    @commands.slash_command(
        default_member_permissions=disnake.Permissions(connect=True)
    )
    async def trade(self, inter):
        pass

    @trade.sub_command(description="Создать запрос на покупку")
    async def buy(self, inter, buy: str):
        await self.__send_trade(inter, "Покупка", f"Куплю: **{buy}**")
        self.logger.info(f"{inter.author} sent a BUY request")

    @trade.sub_command(description="Создать запрос на продажу")
    async def sell(self, inter, sell: str):
        await self.__send_trade(inter, "Продажа", f"Продам: **{sell}**")
        self.logger.info(f"{inter.author} sent a SELL request")

    @trade.sub_command(description="Создать запрос на обмен")
    async def barter(self, inter, give: str, get: str):
        await self.__send_trade(inter, "Обмен", f"Отдам: **{give}**\nПолучу: **{get}**")
        self.logger.info(f"{inter.author} sent a BARTER request")
