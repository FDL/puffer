import logging

import disnake
import redis.asyncio as redis
from disnake.ext import commands

import puffer


class VerificationModal(disnake.ui.Modal):
    def __init__(self):
        self.logger = logging.getLogger("puffer.cogs.verification")
        self.fields = {
            "age": "Сколько вам лет?",
            "nickname": "Ваш никнейм в Minecraft?",
            "license": "Имеется ли у вас лицензия игры?",
        }

        super().__init__(
            title="Анкета верификации",
            custom_id="verification",
            components=[
                disnake.ui.TextInput(
                    label=self.fields["age"],
                    placeholder="Возраст",
                    custom_id="age",
                    style=disnake.TextInputStyle.short,
                    max_length=3,
                ),
                disnake.ui.TextInput(
                    label=self.fields["nickname"],
                    placeholder="Никнейм (С УЧЁТОМ РЕГИСТРА)",
                    custom_id="nickname",
                    style=disnake.TextInputStyle.short,
                    max_length=16,
                ),
                disnake.ui.TextInput(
                    label=self.fields["license"],
                    placeholder="Да/Нет",
                    custom_id="license",
                    style=disnake.TextInputStyle.short,
                    max_length=3,
                ),
            ],
        )

    async def callback(self, inter: disnake.ModalInteraction):
        embed = disnake.Embed(
            title=f"Заявка от `{inter.author}`", color=puffer.config.COLOR
        )
        for key, value in inter.text_values.items():
            embed.add_field(
                name=self.fields[key],
                value=value,
                inline=False,
            )

        await inter.response.defer(ephemeral=True)

        channel = inter.bot.get_channel(puffer.config.MODMAIL_CHANNEL_ID)
        message = await channel.send(
            embed=embed,
            components=[
                disnake.ui.Button(
                    label="Принять",
                    style=disnake.ButtonStyle.success,
                    custom_id=f"accept/{inter.author.id}",
                ),
                disnake.ui.Button(
                    label="Отклонить",
                    style=disnake.ButtonStyle.danger,
                    custom_id=f"decline/{inter.author.id}",
                ),
            ],
        )
        await message.create_thread(name=f"Обсуждение заявки")
        await inter.edit_original_response(
            "Заявка успешно подана, просим вас подождать проверки заявки администратором"
        )
        self.logger.info(f"{inter.author} sent a verification application")


class DeclineReasonModal(disnake.ui.Modal):
    def __init__(self):
        components = [
            disnake.ui.TextInput(
                label="Причина отклонения",
                custom_id="reason",
                style=disnake.TextInputStyle.short,
            ),
        ]
        super().__init__(
            title="Отклонение заявки",
            custom_id="decline",
            components=components,
        )

    async def callback(self, inter: disnake.ModalInteraction):
        await inter.response.defer(with_message=False)


class Verification(commands.Cog):
    def __init__(self, db: redis.Redis, bot: commands.Bot):
        self.logger = logging.getLogger("puffer.cogs.verification")
        self.bot = bot
        self.db = db

    @commands.Cog.listener()
    async def on_ready(self):
        if await self.db.get("nasral") is None:
            ch = self.bot.get_channel(puffer.config.VERIFICATION_CHANNEL_ID)
            await ch.send(
                "Приветствуем!\n"
                + "Добро пожаловать на сервер FDL, первым делом ознакомьтесь с правилами (<#1043889993619353680>) и информацией (<#1040912906767831100>).\n"
                + "Для получения доступа к серверу необходимо заполнить короткую анкету для верификации.\n"
                + "После подачи заявки она обязательно будет рассмотрена администраторами.",
                components=[
                    disnake.ui.Button(
                        label="Подать заявку",
                        style=disnake.ButtonStyle.primary,
                        custom_id="verification",
                    ),
                ],
            )

            await self.db.set("nasral", 1)
            self.logger.info(f"Bot sent the verification request message")

    @commands.Cog.listener()
    async def on_button_click(self, inter: disnake.MessageInteraction):
        if inter.component.custom_id == "verification":
            await inter.response.send_modal(VerificationModal())

        elif inter.component.custom_id.startswith("accept/"):
            user = inter.bot.get_user(
                int(inter.component.custom_id.split("/")[1]),
            )
            await inter.response.defer()
            await inter.guild.get_member(user.id).add_roles(
                inter.guild.get_role(puffer.config.PLAYER_ROLE_ID)
            )
            try:
                await user.send("Ваша заявка была успешно принята! Приятной игры!")
            except:
                self.logger.info(f"Could not send message to {user}")
            await inter.edit_original_message(
                f"`{inter.author}` принял игрока",
                components=None,
            )
            self.logger.info(f"{inter.author} accepted {user}")

        elif inter.component.custom_id.startswith("decline/"):
            await inter.response.send_modal(DeclineReasonModal())
            modal_inter: disnake.ModalInteraction = await inter.bot.wait_for(
                "modal_submit",
                check=lambda x: (
                    (x.custom_id == "decline") and (x.author.id == inter.author.id)
                ),
                timeout=300,
            )
            reason = modal_inter.text_values["reason"]

            user = inter.bot.get_user(
                int(inter.component.custom_id.split("/")[1]),
            )
            try:
                await user.send(f"Увы, вашу заявку отклонили. Причина: `{reason}`")
            except:
                await self.bot.get_channel(puffer.config.VERIFICATION_CHANNEL_ID).send(
                    f"{user.mention}, ваша заявка была отклонена по причине `{reason}`"
                    + "\nВы можете отправить заявку снова, чтобы поправить информацию."
                )
            await inter.edit_original_message(
                f"`{inter.author}` не принял игрока по причине: `{reason}`",
                components=None,
            )
            self.logger.info(
                f"{inter.author} didn't accept {user} w/ reason '{reason}'"
            )
