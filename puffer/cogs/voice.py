import logging

import disnake
import redis.asyncio as redis
from disnake.ext import commands

import puffer


class Voice(commands.Cog):
    def __init__(self, db):
        self.logger = logging.getLogger("puffer.cogs.voice")
        self.db: redis.Redis = db

    @commands.Cog.listener()
    async def on_voice_state_update(
        self,
        member: disnake.Member,
        before: disnake.VoiceState,
        after: disnake.VoiceState,
    ):
        if after.channel is not None:
            if after.channel.id == puffer.config.VOICE_CHANNEL_ID:
                if await self.db.hexists("vcnames", member.id):
                    cname = await self.db.hget("vcnames", member.id)
                else:
                    cname = f"{member.name}'s VC"
                new_channel: disnake.VoiceChannel = (
                    await member.guild.create_voice_channel(
                        cname, category=after.channel.category
                    )
                )
                await member.move_to(new_channel)
                await new_channel.set_permissions(member, manage_channels=True)
                await self.db.sadd("vcs", new_channel.id)
                self.logger.info(f"{new_channel} created by {member}")
        if before.channel is not None:
            if len(before.channel.members) == 0:
                if await self.db.sismember("vcs", before.channel.id):
                    await before.channel.delete()
                    await self.db.srem("vcs", before.channel.id)  # Basedge
                    self.logger.info(f"{before.channel} deleted")

    @commands.slash_command(
        description="Изменить название создаваемого войсчата",
        default_member_permissions=disnake.Permissions(connect=True),
    )
    async def vcname(self, inter, name: str):
        await self.db.hset("vcnames", key=inter.author.id, value=name)
        await inter.send(
            f"Название создаваемого вами войсчата изменено на `{name}`", ephemeral=True
        )
        self.logger.info(f"{inter.author} changed their VC name to {name}")
