from os import environ
from dotenv import load_dotenv

load_dotenv()

TOKEN: str = environ["TOKEN"]
ACTIVITY_NAME: str = environ["ACTIVITY_NAME"]
COLOR: int = int(environ["COLOR"], 16)
WARN_LIMIT: int = int(environ["WARN_LIMIT"])
GUILD_ID: int = int(environ["GUILD_ID"])
TRADE_CHANNEL_ID: int = int(environ["TRADE_CHANNEL_ID"])
VOICE_CHANNEL_ID: int = int(environ["VOICE_CHANNEL_ID"])
VERIFICATION_CHANNEL_ID: int = int(environ["VERIFICATION_CHANNEL_ID"])
MODMAIL_CHANNEL_ID: int = int(environ["MODMAIL_CHANNEL_ID"])
PLAYER_ROLE_ID: int = int(environ["PLAYER_ROLE_ID"])
REDIS_URL: str = environ["REDIS_URL"]
ENABLED_COGS: list = environ["ENABLED_COGS"].split(" ")

__all__ = [
    "TOKEN",
    "ACTIVITY_NAME",
    "COLOR",
    "WARN_LIMIT",
    "GUILD_ID",
    "TRADE_CHANNEL_ID",
    "VOICE_CHANNEL_ID",
    "VERIFICATION_CHANNEL_ID",
    "MODMAIL_CHANNEL_ID",
    "PLAYER_ROLE_ID",
    "REDIS_URL",
    "ENABLED_COGS",
]
