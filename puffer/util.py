import logging

from colorama import Fore
from colorama import init as init_colorama

init_colorama()


class CustomFormatter(logging.Formatter):
    FORMAT = (
        "[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s (%(filename)s:%(lineno)d)"
    )

    COLORS = {
        logging.DEBUG: Fore.WHITE,
        logging.INFO: Fore.LIGHTBLUE_EX,
        logging.WARNING: Fore.YELLOW,
        logging.ERROR: Fore.LIGHTRED_EX,
        logging.CRITICAL: Fore.RED,
    }

    def format(self, record):
        fmt = self.COLORS[record.levelno] + self.FORMAT + Fore.RESET
        formatter = logging.Formatter(fmt)
        return formatter.format(record)


def setup_logging():
    init_colorama()

    root = logging.getLogger()

    root.setLevel(logging.WARNING)
    handler = logging.StreamHandler()
    handler.setFormatter(CustomFormatter())
    root.addHandler(handler)

    logging.getLogger("puffer").setLevel(logging.DEBUG)
